package com.chanshuyi.service;

import com.chanshuyi.model.SysCode;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadFileTest {

    ApplicationContext ctx = null;

    ISysCodeService sysCodeService;

    @Before
    public void init() {
        ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        sysCodeService = (ISysCodeService) ctx.getBean("sysCodeService");
    }

    @Test
    public void readFile() throws  Exception{

        Map<String, String> provinceMap = new HashMap<>();
        Map<String, String> cityMap = new HashMap<>();
        Map<String, String> districtMap = new HashMap<>();

        Map<String, SysCode> provinceList = new HashMap<>();
        Map<String, SysCode> cityList = new HashMap<>();
        Map<String, SysCode> districtList = new HashMap<>();


        String provinceRegex = ".*0000.*";
        String cityRegex = ".*\\d{4}00.*";

        String fileName = "E:\\Hello.md";
        try{
            InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName));
            BufferedReader reader = new BufferedReader(isr);
            String str = "";
            String latestProvince= "-1";
            String latestCity = "-1";
            while((str = reader.readLine()) != null && str.length() != 0){
                if (str.matches(provinceRegex)) {
                    String[] result = getArray(str);
                    provinceMap.put(result[0], result[1]);

                    SysCode sysCode = new SysCode();
                    sysCode.setCodeId(Integer.parseInt(result[0].trim()));
                    sysCode.setCodeName(result[1]);
                    sysCode.setCodeTypeId(2);
                    sysCode.setParentCodeId(-1);
                    provinceList.put(result[0], sysCode);
                    latestProvince = result[0];
                } else if (str.matches(cityRegex) && !str.matches(provinceRegex)) {
                    String[] result = getArray(str);
                    cityMap.put(result[0], result[1]);

                    SysCode sysCode = new SysCode();
//                    if (result[0].contains("110100")) {
//                        System.out.println("stop");
//                    }
                    sysCode.setCodeId(Integer.parseInt(result[0].replace("\\s", "")));
                    sysCode.setCodeName(result[1]);
                    sysCode.setCodeTypeId(3);
                    sysCode.setParentCodeId(Integer.parseInt(latestProvince));
                    cityList.put(result[0], sysCode);
                    latestCity = result[0];
                } else{
                    String[] result = getArray(str);
                    districtMap.put(result[0], result[1]);

                    SysCode sysCode = new SysCode();
                    sysCode.setCodeId(Integer.parseInt(result[0].trim()));
                    sysCode.setCodeName(result[1]);
                    sysCode.setCodeTypeId(4);
                    sysCode.setParentCodeId(Integer.parseInt(latestCity));
                    districtList.put(result[0], sysCode);
                }
            }

            //插入数据库表
//            for (String key : districtList.keySet()) {
//                SysCode districtSysCode = districtList.get(key);
//                String district = districtSysCode.getCodeName();
//
//                SysCode citySysCode = cityList.get(districtSysCode.getParentCodeId() + "");
//                String city = citySysCode.getCodeName();
//
//                SysCode provinceSysCode = provinceList.get(citySysCode.getParentCodeId() + "");
//                String province = provinceSysCode.getCodeName();
//
//                System.out.println(province + ">>" + city + ">>" + district);
//            }

            for (SysCode sysCode : provinceList.values()) {
//                sysCodeService.insert(sysCode);
                System.out.println("insert into SysCode values('" + sysCode.getCodeId() + "','" + sysCode.getCodeTypeId() + "','" + sysCode.getCodeName() + "','" + sysCode.getParentCodeId() + "');");
            }

            for (SysCode sysCode : cityList.values()) {
//                sysCodeService.insert(sysCode);
                System.out.println("insert into SysCode values('" + sysCode.getCodeId() + "','" + sysCode.getCodeTypeId() + "','" + sysCode.getCodeName() + "','" + sysCode.getParentCodeId() + "');");
            }

            for (SysCode sysCode : districtList.values()) {
//                sysCodeService.insert(sysCode);
                System.out.println("insert into SysCode values('" + sysCode.getCodeId() + "','" + sysCode.getCodeTypeId() + "','" + sysCode.getCodeName() + "','" + sysCode.getParentCodeId() + "');");
            }


//            print(provinceMap);
//            print(cityMap);
//            print(districtMap);
            reader.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String extractNumFromStr(String string) {
        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(string);
        return m.replaceAll("").trim();
    }

    public void print(Map<String, String> map) {
        for (String key : map.keySet()) {
            System.out.println(key + ">>>" + map.get(key));
        }
        System.out.println("===============================");
    }

    public String[] getArray(String string) {
        String[] array = string.split("\\s");
        String[] result = new String[2];
        result[0] = extractNumFromStr(array[0]);
        result[1] = array[array.length - 1];
        return result;
    }
}
