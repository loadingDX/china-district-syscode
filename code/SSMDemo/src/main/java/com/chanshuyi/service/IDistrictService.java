package com.chanshuyi.service;

import com.chanshuyi.model.District;

import java.util.List;
import java.util.Map;

public interface IDistrictService {
    List<District> getUserListByMapSql(Map<String, String> param);
}
