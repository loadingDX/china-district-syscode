package com.chanshuyi.model;

public class SysCode {
    /**
     *
     */
    private Integer codeId;

    /**
     *编码类别
     */
    private Integer codeTypeId;

    /**
     *
     */
    private String codeName;

    /**
     *
     */
    private Integer parentCodeId;

    /**
     *
     */
    public Integer getCodeId() {
        return codeId;
    }

    /**
     *
     */
    public void setCodeId(Integer codeId) {
        this.codeId = codeId;
    }

    /**
     *编码类别
     */
    public Integer getCodeTypeId() {
        return codeTypeId;
    }

    /**
     *编码类别
     */
    public void setCodeTypeId(Integer codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    /**
     *
     */
    public String getCodeName() {
        return codeName;
    }

    /**
     *
     */
    public void setCodeName(String codeName) {
        this.codeName = codeName == null ? null : codeName.trim();
    }

    /**
     *
     */
    public Integer getParentCodeId() {
        return parentCodeId;
    }

    /**
     *
     */
    public void setParentCodeId(Integer parentCodeId) {
        this.parentCodeId = parentCodeId;
    }
}